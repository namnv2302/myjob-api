import { Status } from '@resumes/entities/resume.entity';
import { IUser } from '@users/users.interface';
import { IsEmail, IsNotEmpty } from 'class-validator';

export class CreateResumeDto {
  @IsNotEmpty()
  @IsEmail()
  email: string;

  @IsNotEmpty()
  fullname: string;

  @IsNotEmpty()
  fileUrl: string;

  status: Status;

  sendBy: IUser;

  @IsNotEmpty()
  job: number;
}
