import { PartialType } from '@nestjs/swagger';
import { CreateResumeDto } from '@resumes/dto/create-resume.dto';

export class UpdateResumeDto extends PartialType(CreateResumeDto) {}
