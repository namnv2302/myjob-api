import { Gateway } from '@/gateway/gateway';
import { NotificationsService } from '@/notifications/notifications.service';
import { Job } from '@jobs/entities/job.entity';
import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateResumeDto } from '@resumes/dto/create-resume.dto';
import { UpdateResumeDto } from '@resumes/dto/update-resume.dto';
import { Resume, Status } from '@resumes/entities/resume.entity';
import { IUser } from '@users/users.interface';
import aqp from 'api-query-params';
import { Repository } from 'typeorm';

@Injectable()
export class ResumesService {
  constructor(
    @InjectRepository(Resume) private resumesRepository: Repository<Resume>,
    @InjectRepository(Job) private jobsRepository: Repository<Job>,
    private gateway: Gateway,
    private notificationsService: NotificationsService,
  ) {}

  async create(createResumeDto: CreateResumeDto, user: IUser) {
    try {
      const job = await this.jobsRepository.findOneBy({
        id: createResumeDto.job,
      });
      delete createResumeDto.job;
      const resume = await this.resumesRepository.create({
        fullname: createResumeDto.fullname,
        email: createResumeDto.email,
        status: createResumeDto.status,
        fileUrl: createResumeDto.fileUrl,
        sendBy: user,
      });
      resume.job = job;
      await this.resumesRepository.save(resume);
      return 'OK';
    } catch (error) {
      throw new BadRequestException('Server failure, try again');
    }
  }

  async findAll(currentPage: number, limit: number, queryString: string) {
    const { filter } = aqp(queryString);
    delete filter.current;
    delete filter.pageSize;

    filter.companyId = filter.companyId ? filter.companyId : false;

    const defaultLimit = +limit ? +limit : 10;
    const offset = ((+currentPage || 1) - 1) * +defaultLimit;
    const totalItems = (
      filter.companyId
        ? await this.resumesRepository.find({
            where: {
              job: { company: { id: filter.companyId } },
            },
          })
        : await this.resumesRepository.find({
            where: {},
          })
    ).length;
    const totalPages = Math.ceil(totalItems / defaultLimit);

    const resumes = filter.companyId
      ? await this.resumesRepository.find({
          where: {
            job: { company: { id: filter.companyId } },
          },
          order: {
            createdAt: 'DESC',
          },
          skip: offset,
          take: defaultLimit,
          relations: ['job', 'sendBy'],
        })
      : await this.resumesRepository.find({
          where: {},
          order: {
            createdAt: 'DESC',
          },
          skip: offset,
          take: defaultLimit,
          relations: ['job', 'sendBy'],
        });
    return {
      data: resumes,
      meta: {
        current: currentPage || 1,
        pageSize: defaultLimit,
        pages: totalPages,
        total: totalItems,
      },
    };
  }

  async getInfoAll(companyId: number) {
    const status0 = await this.resumesRepository.count({
      where: {
        job: { company: { id: companyId } },
        status: Status.Pending,
      },
    });
    const status1 = await this.resumesRepository.count({
      where: {
        job: { company: { id: companyId } },
        status: Status.Accept,
      },
    });
    const status2 = await this.resumesRepository.count({
      where: {
        job: { company: { id: companyId } },
        status: Status.Reject,
      },
    });
    return { status0, status1, status2 };
  }

  findOne(id: number) {
    return `This action returns a #${id} resume`;
  }

  async sended(user: IUser) {
    return await this.resumesRepository.find({
      where: { email: user.email },
      relations: ['job'],
    });
  }

  async update(id: number, updateResumeDto: UpdateResumeDto) {
    try {
      const resume = await this.resumesRepository.findOne({
        where: { id },
        relations: ['sendBy', 'job', 'job.company'],
      });
      const newNoti = await this.notificationsService.create({
        content: `${updateResumeDto.status}`,
        sender: resume.job.company.id,
        recipient: resume.sendBy.id,
        isRead: false,
      });

      this.gateway.sendNotification(
        +newNoti.id,
        +resume.sendBy.id,
        +resume.job.company.id,
        +updateResumeDto.status,
      );

      return await this.resumesRepository.save({
        ...resume,
        status: updateResumeDto.status,
      });
    } catch (error) {
      console.log(error);
      throw new BadRequestException('Server failure, try again');
    }
  }

  remove(id: number) {
    return `This action removes a #${id} resume`;
  }
}
