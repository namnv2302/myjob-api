import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Query,
} from '@nestjs/common';
import { ResumesService } from '@resumes/resumes.service';
import { CreateResumeDto } from '@resumes/dto/create-resume.dto';
import { UpdateResumeDto } from '@resumes/dto/update-resume.dto';
import { RolesGuard } from '@/guards/roles.guard';
import { Roles } from '@/decorator/roles';
import { Role } from '@users/entities/user.entity';
import { User } from '@/decorator/user';
import { IUser } from '@users/users.interface';

@Controller('resumes')
export class ResumesController {
  constructor(private readonly resumesService: ResumesService) {}

  @Post()
  create(@Body() createResumeDto: CreateResumeDto, @User() user: IUser) {
    return this.resumesService.create(createResumeDto, user);
  }

  @Get()
  @UseGuards(RolesGuard)
  @Roles(Role.Employer, Role.Admin)
  findAll(
    @Query('current') currentPage: string,
    @Query('limit') limit: string,
    @Query() queryString: string,
  ) {
    return this.resumesService.findAll(+currentPage, +limit, queryString);
  }

  @Get('info-all')
  @UseGuards(RolesGuard)
  @Roles(Role.Employer)
  getInfoAll(@Query('companyId') companyId: number) {
    return this.resumesService.getInfoAll(companyId);
  }

  @Get('sended')
  sended(@User() user: IUser) {
    return this.resumesService.sended(user);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.resumesService.findOne(+id);
  }

  @Patch(':id')
  @UseGuards(RolesGuard)
  @Roles(Role.Employer, Role.Admin)
  update(@Param('id') id: string, @Body() updateResumeDto: UpdateResumeDto) {
    return this.resumesService.update(+id, updateResumeDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.resumesService.remove(+id);
  }
}
