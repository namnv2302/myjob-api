import { User } from '@users/entities/user.entity';
import { IUser } from '@users/users.interface';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('posts')
export class Post {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true, type: 'text' })
  title: string;

  @Column({ nullable: true, type: 'text' })
  content: string;

  @ManyToOne(() => User, (user) => user.posts)
  owner: User;

  @ManyToMany(() => User, (user) => user.likedPostList)
  @JoinTable()
  likedList: IUser[];

  addUserToLikedList(user: IUser) {
    if (this.likedList === undefined || this.likedList === null) {
      this.likedList = new Array<IUser>();
    }
    this.likedList.push(user);
  }

  @ManyToMany(() => User, (user) => user.bookmarkPostList)
  @JoinTable()
  bookmarkList: IUser[];

  addUserToBookmarkList(user: IUser) {
    if (this.bookmarkList === undefined || this.bookmarkList === null) {
      this.bookmarkList = new Array<IUser>();
    }
    this.bookmarkList.push(user);
  }

  @CreateDateColumn({ nullable: true })
  createdAt: Date;

  @UpdateDateColumn({ nullable: true })
  updatedAt: Date;

  @DeleteDateColumn({ nullable: true })
  deletedAt: Date;
}
