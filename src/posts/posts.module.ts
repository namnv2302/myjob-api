import { Module } from '@nestjs/common';
import { PostsService } from '@/posts/posts.service';
import { PostsController } from '@/posts/posts.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Post } from '@/posts/entities/post.entity';
import { User } from '@users/entities/user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Post, User])],
  controllers: [PostsController],
  providers: [PostsService],
})
export class PostsModule {}
