import { BadRequestException, Injectable } from '@nestjs/common';
import { CreatePostDto } from '@/posts/dto/create-post.dto';
// import { UpdatePostDto } from '@/posts/dto/update-post.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Post } from '@/posts/entities/post.entity';
import { Like, Repository } from 'typeorm';
import { User } from '@users/entities/user.entity';
import aqp from 'api-query-params';
import { IUser } from '@users/users.interface';

@Injectable()
export class PostsService {
  constructor(
    @InjectRepository(Post) private postsRepository: Repository<Post>,
    @InjectRepository(User) private usersRepository: Repository<User>,
  ) {}

  async create(createPostDto: CreatePostDto) {
    try {
      const owner = await this.usersRepository.findOneBy({
        id: createPostDto.owner,
      });
      delete createPostDto.owner;
      const post = await this.postsRepository.create({
        title: createPostDto.title,
        content: createPostDto.content,
      });
      post.owner = owner;
      const resp = await this.postsRepository.save(post);
      if (resp) {
        return 'OK';
      } else {
        throw new BadRequestException('Failure, try again');
      }
    } catch (error) {
      throw new BadRequestException('Server failure, try again');
    }
  }

  async findAll(currentPage: number, limit: number, queryString: string) {
    const { filter } = aqp(queryString);
    delete filter.current;
    delete filter.pageSize;

    filter.title = filter.title ? filter.title : '';
    filter.order = filter.order ? filter.order : 'DESC';

    const defaultLimit = +limit ? +limit : 10;
    const offset = ((+currentPage || 1) - 1) * +defaultLimit;
    const totalItems = (
      await this.postsRepository.find({
        where: {
          title: Like(`%${filter.title}%`),
        },
      })
    ).length;
    const totalPages = Math.ceil(totalItems / defaultLimit);

    const posts = await this.postsRepository.find({
      where: {
        title: Like(`%${filter.title}%`),
      },
      order: {
        createdAt: filter.order,
      },
      skip: offset,
      take: defaultLimit,
      relations: ['owner'],
    });
    return {
      data: posts,
      meta: {
        current: currentPage || 1,
        pageSize: defaultLimit,
        pages: totalPages,
        total: totalItems,
      },
    };
  }

  async findOne(id: number) {
    try {
      const post = await this.postsRepository.findOne({
        where: { id },
        relations: ['owner', 'bookmarkList'],
      });
      return post;
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }

  remove(id: number) {
    try {
      this.postsRepository.softDelete(id);
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }

  async toogleBookmark(id: number, user: IUser) {
    try {
      const post = await this.postsRepository.findOne({
        where: { id },
        relations: ['bookmarkList'],
      });
      const isBookmark = post.bookmarkList.find((u) => u.id === user.id);
      if (isBookmark) {
        post.bookmarkList = post.bookmarkList.filter((u) => u.id !== user.id);
        await this.postsRepository.save(post);
        return { newValue: false };
      } else {
        post.addUserToBookmarkList(user);
        await this.postsRepository.save(post);
        return { newValue: true };
      }
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }

  async mePosts(user: IUser) {
    try {
      const posts = await this.postsRepository.find({
        where: { owner: { id: user.id } },
        relations: ['owner'],
      });
      return {
        data: posts,
      };
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }
}
