import {
  Controller,
  Get,
  Post,
  Body,
  // Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { PostsService } from '@/posts/posts.service';
import { CreatePostDto } from '@/posts/dto/create-post.dto';
// import { UpdatePostDto } from '@/posts/dto/update-post.dto';
import { Public } from '@/decorator/customize';
import { User } from '@/decorator/user';
import { IUser } from '@users/users.interface';

@Controller('posts')
export class PostsController {
  constructor(private readonly postsService: PostsService) {}

  @Post()
  create(@Body() createPostDto: CreatePostDto) {
    return this.postsService.create(createPostDto);
  }

  @Public()
  @Get()
  findAll(
    @Query('current') currentPage: string,
    @Query('limit') limit: string,
    @Query() queryString: string,
  ) {
    return this.postsService.findAll(+currentPage, +limit, queryString);
  }

  @Get('me')
  mePosts(@User() user: IUser) {
    return this.postsService.mePosts(user);
  }

  @Public()
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.postsService.findOne(+id);
  }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updatePostDto: UpdatePostDto) {
  //   return this.postsService.update(+id, updatePostDto);
  // }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.postsService.remove(+id);
  }

  @Get(':id/toggle-bookmark')
  toogleBookmark(@Param('id') id: string, @User() user: IUser) {
    return this.postsService.toogleBookmark(+id, user);
  }
}
