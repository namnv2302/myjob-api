import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  UseGuards,
} from '@nestjs/common';
import { CompaniesService } from '@/companies/companies.service';
import { CreateCompanyDto } from '@/companies/dto/create-company.dto';
import { UpdateCompanyDto } from '@/companies/dto/update-company.dto';
import { ApiTags } from '@nestjs/swagger';
import { RolesGuard } from '@/guards/roles.guard';
import { Roles } from '@/decorator/roles';
import { Role } from '@users/entities/user.entity';
import { Public } from '@/decorator/customize';
import { User } from '@/decorator/user';
import { IUser } from '@users/users.interface';

@ApiTags('companies')
@Controller('companies')
export class CompaniesController {
  constructor(private readonly companiesService: CompaniesService) {}

  @Post()
  create(@Body() createCompanyDto: CreateCompanyDto) {
    return this.companiesService.create(createCompanyDto);
  }

  @Public()
  @Get()
  findAll(
    @Query('current') currentPage: string,
    @Query('limit') limit: string,
    @Query() queryString: string,
  ) {
    return this.companiesService.findAll(+currentPage, +limit, queryString);
  }

  @Public()
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.companiesService.findOne(+id);
  }

  @Patch(':id')
  @UseGuards(RolesGuard)
  @Roles(Role.Employer, Role.Admin)
  update(@Param('id') id: string, @Body() updateCompanyDto: UpdateCompanyDto) {
    return this.companiesService.update(+id, updateCompanyDto);
  }

  @Delete(':id')
  @UseGuards(RolesGuard)
  @Roles(Role.Employer, Role.Admin)
  remove(@Param('id') id: string) {
    return this.companiesService.remove(+id);
  }

  @Get(':id/follow')
  follow(@Param('id') id: string, @User() user: IUser) {
    return this.companiesService.follow(+id, user);
  }

  @Get(':id/unfollow')
  unfollow(@Param('id') id: string, @User() user: IUser) {
    return this.companiesService.unfollow(+id, user);
  }
}
