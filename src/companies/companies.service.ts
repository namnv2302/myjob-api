import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateCompanyDto } from '@companies/dto/create-company.dto';
import { UpdateCompanyDto } from '@companies/dto/update-company.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Company } from '@companies/entities/company.entity';
import { Like, Not, Repository } from 'typeorm';
import aqp from 'api-query-params';
import { IUser } from '@users/users.interface';

@Injectable()
export class CompaniesService {
  constructor(
    @InjectRepository(Company) private companiesRepository: Repository<Company>,
  ) {}

  async create(createCompanyDto: CreateCompanyDto) {
    const isExist = await this.companiesRepository.findOne({
      where: {
        companyName: createCompanyDto.companyName.toUpperCase(),
      },
      withDeleted: true,
    });
    if (isExist) {
      throw new BadRequestException('Company already exist!');
    }
    try {
      return await this.companiesRepository.save({
        ...createCompanyDto,
        companyName: createCompanyDto.companyName.toUpperCase(),
      });
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }

  async findAll(currentPage: number, limit: number, queryString: string) {
    const { filter } = aqp(queryString);
    delete filter.current;
    delete filter.pageSize;

    filter.companyName = filter.companyName ? filter.companyName : '';
    filter.provinceName = filter.provinceName ? filter.provinceName : '';
    filter.districtName = filter.districtName ? filter.districtName : '';

    const defaultLimit = +limit ? +limit : 10;
    const offset = ((+currentPage || 1) - 1) * +defaultLimit;
    const totalItems = (
      await this.companiesRepository.find({
        where: {
          companyName: Like(`%${filter.companyName}%`),
          provinceName: Like(`%${filter.provinceName}%`),
          districtName: Like(`%${filter.districtName}%`),
        },
      })
    ).length;
    const totalPages = Math.ceil(totalItems / defaultLimit);

    const companies = await this.companiesRepository.find({
      where: {
        companyName: Like(`%${filter.companyName}%`),
        provinceName: Like(`%${filter.provinceName}%`),
        districtName: Like(`%${filter.districtName}%`),
      },
      order: {
        createdAt: 'DESC',
      },
      skip: offset,
      take: defaultLimit,
      relations: ['users', 'jobs'],
    });
    return {
      data: companies,
      meta: {
        current: currentPage || 1,
        pageSize: defaultLimit,
        pages: totalPages,
        total: totalItems,
      },
    };
  }

  async findOne(id: number) {
    try {
      const company = await this.companiesRepository.findOne({
        where: { id },
        relations: ['users', 'jobs', 'followers'],
      });
      return company;
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }

  async update(id: number, updateCompanyDto: UpdateCompanyDto) {
    const isExist = await this.companiesRepository.findOne({
      where: {
        companyName: updateCompanyDto.companyName.toUpperCase(),
        id: Not(id),
      },
      withDeleted: true,
    });
    if (isExist) {
      throw new BadRequestException('Company already exist!');
    }
    try {
      const company = await this.companiesRepository.findOneBy({ id });

      return await this.companiesRepository.save({
        ...company,
        ...updateCompanyDto,
        companyName: updateCompanyDto.companyName.toUpperCase(),
      });
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }

  remove(id: number) {
    try {
      this.companiesRepository.softDelete(id);
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }

  async follow(id: number, user: IUser) {
    try {
      const company = await this.companiesRepository.findOne({
        where: { id },
        relations: ['followers'],
      });
      company.addUserToFollowersList(user);
      return await this.companiesRepository.save(company);
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }

  async unfollow(id: number, user: IUser) {
    try {
      const company = await this.companiesRepository.findOne({
        where: { id },
        relations: ['followers'],
      });
      company.followers = company.followers.filter(
        (item) => item.id !== user.id,
      );
      return await this.companiesRepository.save(company);
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }
}
