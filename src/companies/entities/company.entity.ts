import { Notification } from '@/notifications/entities/notification.entity';
import { Job } from '@jobs/entities/job.entity';
import { User } from '@users/entities/user.entity';
import { IUser } from '@users/users.interface';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('companies')
export class Company {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true })
  companyName: string;

  @Column({ nullable: true })
  provinceName: string;

  @Column({ nullable: true })
  districtName: string;

  @Column({ type: 'text', nullable: true })
  introduction: string;

  @Column({ type: 'text', nullable: true })
  logo: string;

  @Column({ nullable: true })
  scales: string;

  @OneToMany(() => User, (user) => user.company)
  users: User[];

  @OneToMany(() => Job, (job) => job.company)
  jobs: Job[];

  @ManyToMany(() => User, (user) => user.companiesFollowed)
  @JoinTable()
  followers: IUser[];

  addUserToFollowersList(user: IUser) {
    if (this.followers === undefined || this.followers === null) {
      this.followers = new Array<IUser>();
    }
    this.followers.push(user);
  }

  @OneToMany(() => Notification, (noti) => noti.sender)
  notis: Notification;

  @CreateDateColumn({ nullable: true })
  createdAt: Date;

  @UpdateDateColumn({ nullable: true })
  updatedAt: Date;

  @DeleteDateColumn({ nullable: true })
  deletedAt: Date;
}
