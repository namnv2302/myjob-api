import { Module } from '@nestjs/common';
import { NotificationsService } from '@/notifications/notifications.service';
import { NotificationsController } from '@/notifications/notifications.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Notification } from '@/notifications/entities/notification.entity';
import { Company } from '@companies/entities/company.entity';
import { User } from '@users/entities/user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Notification, Company, User])],
  controllers: [NotificationsController],
  providers: [NotificationsService],
  exports: [NotificationsService],
})
export class NotificationsModule {}
