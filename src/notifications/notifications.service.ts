import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateNotificationDto } from '@/notifications/dto/create-notification.dto';
import { UpdateNotificationDto } from '@/notifications/dto/update-notification.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Notification } from '@/notifications/entities/notification.entity';
import { Repository } from 'typeorm';
import { Company } from '@companies/entities/company.entity';
import { User } from '@users/entities/user.entity';

@Injectable()
export class NotificationsService {
  constructor(
    @InjectRepository(Notification)
    private notificationsRepository: Repository<Notification>,
    @InjectRepository(Company)
    private companiesRepository: Repository<Company>,
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}

  async create(createNotificationDto: CreateNotificationDto) {
    try {
      const { sender, recipient, content } = createNotificationDto;
      const senderObj = await this.companiesRepository.findOneBy({
        id: sender,
      });
      const recipientObj = await this.usersRepository.findOneBy({
        id: recipient,
      });
      const newNoti = await this.notificationsRepository.create({
        content,
      });
      newNoti.sender = senderObj;
      newNoti.recipient = recipientObj;
      return await this.notificationsRepository.save(newNoti);
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }

  findAll() {
    return `This action returns all notifications`;
  }

  async findUserNotis(id: number, currentPage: number, limit: number) {
    const defaultLimit = +limit ? +limit : 10;
    const offset = ((+currentPage || 1) - 1) * +defaultLimit;
    const totalItems = (
      await this.notificationsRepository.find({
        where: [
          {
            recipient: { id: id },
          },
        ],
      })
    ).length;
    const totalPages = Math.ceil(totalItems / defaultLimit);

    try {
      const notis = await this.notificationsRepository.find({
        where: [
          {
            recipient: { id: id },
          },
        ],
        order: { createdAt: 'DESC' },
        skip: offset,
        take: defaultLimit,
        relations: ['sender'],
      });
      return {
        data: notis,
        meta: {
          current: currentPage || 1,
          pageSize: defaultLimit,
          pages: totalPages,
          total: totalItems,
        },
      };
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }

  findOne(id: number) {
    return `This action returns a #${id} notification`;
  }

  async update(id: number, updateNotificationDto: UpdateNotificationDto) {
    try {
      const noti = await this.notificationsRepository.findOneBy({ id });
      return await this.notificationsRepository.save({
        ...noti,
        isRead: updateNotificationDto.isRead,
      });
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }

  remove(id: number) {
    return `This action removes a #${id} notification`;
  }
}
