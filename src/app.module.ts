import { Module } from '@nestjs/common';
import { AppController } from '@/app.controller';
import { AppService } from '@/app.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from '@/users/users.module';
import { User } from '@users/entities/user.entity';
import { Company } from '@/companies/entities/company.entity';
import { AuthModule } from '@auth/auth.module';
import { CompaniesModule } from '@companies/companies.module';
import { ThrottlerModule } from '@nestjs/throttler';
import { MailModule } from '@/mail/mail.module';
import { CloudinaryModule } from '@/cloudinary/cloudinary.module';
import { FilesModule } from '@/files/files.module';
import { JobsModule } from '@jobs/jobs.module';
import { Job } from '@jobs/entities/job.entity';
import { ResumesModule } from '@resumes/resumes.module';
import { Resume } from '@resumes/entities/resume.entity';
import { PostsModule } from '@/posts/posts.module';
import { Post } from '@/posts/entities/post.entity';
import { ChatsModule } from '@chats/chats.module';
import { Chat } from '@chats/entities/chat.entity';
import { MessagesModule } from '@/messages/messages.module';
import { Message } from '@/messages/entities/message.entity';
import { NotificationsModule } from '@/notifications/notifications.module';
import { Notification } from '@/notifications/entities/notification.entity';
import { GatewayModule } from '@/gateway/gateway.module';

@Module({
  imports: [
    ThrottlerModule.forRoot([
      {
        ttl: 60000,
        limit: 5,
      },
    ]),
    ConfigModule.forRoot({ isGlobal: true }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        type: 'mysql',
        host: configService.get<string>('DB_HOST'),
        port: +configService.get<string>('DB_PORT'),
        username: configService.get<string>('DB_USERNAME'),
        password: configService.get<string>('DB_PASSWORD') || null,
        database: configService.get<string>('DB_NAME'),
        entities: [
          User,
          Company,
          Job,
          Resume,
          Post,
          Chat,
          Message,
          Notification,
        ],
        synchronize: true,
      }),
      inject: [ConfigService],
    }),
    UsersModule,
    AuthModule,
    CompaniesModule,
    MailModule,
    CloudinaryModule,
    FilesModule,
    JobsModule,
    ResumesModule,
    TypeOrmModule.forFeature([Company, Job, User]),
    PostsModule,
    ChatsModule,
    MessagesModule,
    NotificationsModule,
    GatewayModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
