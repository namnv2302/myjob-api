import { Module } from '@nestjs/common';
import { JobsService } from '@jobs/jobs.service';
import { JobsController } from '@jobs/jobs.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Job } from '@jobs/entities/job.entity';
import { Company } from '@companies/entities/company.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Job, Company])],
  controllers: [JobsController],
  providers: [JobsService],
})
export class JobsModule {}
