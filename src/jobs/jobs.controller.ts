import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  UseGuards,
} from '@nestjs/common';
import { JobsService } from '@jobs/jobs.service';
import { CreateJobDto } from '@jobs/dto/create-job.dto';
import { UpdateJobDto } from '@jobs/dto/update-job.dto';
import { Public } from '@/decorator/customize';
import { RolesGuard } from '@/guards/roles.guard';
import { Roles } from '@/decorator/roles';
import { Role } from '@users/entities/user.entity';
import { User } from '@/decorator/user';
import { IUser } from '@users/users.interface';

@Controller('jobs')
export class JobsController {
  constructor(private readonly jobsService: JobsService) {}

  @Post()
  @UseGuards(RolesGuard)
  @Roles(Role.Employer, Role.Admin)
  create(@Body() createJobDto: CreateJobDto) {
    return this.jobsService.create(createJobDto);
  }

  @Public()
  @Get()
  findAll(
    @Query('current') currentPage: string,
    @Query('limit') limit: string,
    @Query() queryString: string,
  ) {
    return this.jobsService.findAll(+currentPage, +limit, queryString);
  }

  @UseGuards(RolesGuard)
  @Roles(Role.Employer, Role.Admin)
  @Get('admin')
  findAllJobs(
    @Query('current') currentPage: string,
    @Query('limit') limit: string,
    @Query() queryString: string,
  ) {
    return this.jobsService.findAllJobs(+currentPage, +limit, queryString);
  }

  @Public()
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.jobsService.findOne(+id);
  }

  @Patch(':id')
  @UseGuards(RolesGuard)
  @Roles(Role.Employer, Role.Admin)
  update(@Param('id') id: string, @Body() updateJobDto: UpdateJobDto) {
    return this.jobsService.update(+id, updateJobDto);
  }

  @Delete(':id')
  @UseGuards(RolesGuard)
  @Roles(Role.Employer, Role.Admin)
  remove(@Param('id') id: string) {
    return this.jobsService.remove(+id);
  }

  @Get(':id/like')
  like(@Param('id') id: string, @User() user: IUser) {
    return this.jobsService.like(+id, user);
  }

  @Get(':id/unlike')
  unlike(@Param('id') id: string, @User() user: IUser) {
    return this.jobsService.unlike(+id, user);
  }
}
