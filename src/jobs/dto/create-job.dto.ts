import { Level } from '@jobs/entities/job.entity';
import { Transform } from 'class-transformer';
import { IsArray, IsDate, IsNotEmpty } from 'class-validator';

export class CreateJobDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  description: string;

  @IsNotEmpty()
  @IsArray()
  skills: string[];

  @IsNotEmpty()
  location: string;

  @IsNotEmpty()
  salary: string;

  @IsNotEmpty()
  level: Level;

  @IsNotEmpty()
  quantity: string;

  isActive: boolean;

  @IsNotEmpty()
  company: number;

  @IsNotEmpty()
  @Transform(({ value }) => new Date(value))
  @IsDate()
  startDate: Date;

  @IsNotEmpty()
  @Transform(({ value }) => new Date(value))
  @IsDate()
  endDate: Date;
}
