import { Company } from '@companies/entities/company.entity';
import { Resume } from '@resumes/entities/resume.entity';
import { User } from '@users/entities/user.entity';
import { IUser } from '@users/users.interface';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

export enum Level {
  No = 0,
  Fresher = 1,
  Junior = 2,
  Middle = 3,
  Senior = 4,
  TechLead = 5,
}

@Entity('jobs')
export class Job {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true })
  name: string;

  @Column({ nullable: true, type: 'text' })
  description: string;

  @Column('simple-array', { nullable: true })
  skills: string[];

  @Column({ nullable: true, length: 100 })
  location: string;

  @Column({ nullable: true, length: 100 })
  salary: string;

  @Column({ type: 'enum', enum: Level, default: Level.No })
  level: Level;

  @Column({ nullable: true, length: 100 })
  quantity: string;

  @Column({ type: 'datetime', nullable: true })
  startDate: Date;

  @Column({ type: 'datetime', nullable: true })
  endDate: Date;

  @ManyToOne(() => Company, (company) => company.jobs)
  company: Company;

  @OneToMany(() => Resume, (item) => item.job)
  resumes: Resume[];

  @ManyToMany(() => User, (user) => user.jobLiked)
  @JoinTable()
  liked: IUser[];

  addUserLiked(user: IUser) {
    if (this.liked === undefined || this.liked === null) {
      this.liked = new Array<IUser>();
    }
    this.liked.push(user);
  }

  @Column({ type: 'boolean', nullable: true, default: true })
  isActive: boolean;

  @CreateDateColumn({ nullable: true })
  createdAt: Date;

  @UpdateDateColumn({ nullable: true })
  updatedAt: Date;

  @DeleteDateColumn({ nullable: true })
  deletedAt: Date;
}
