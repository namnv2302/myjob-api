import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateJobDto } from '@jobs/dto/create-job.dto';
import { UpdateJobDto } from '@jobs/dto/update-job.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Job } from '@jobs/entities/job.entity';
import { Like, Repository } from 'typeorm';
import { Company } from '@companies/entities/company.entity';
import aqp from 'api-query-params';
import { IUser } from '@users/users.interface';

@Injectable()
export class JobsService {
  constructor(
    @InjectRepository(Job) private jobsRepository: Repository<Job>,
    @InjectRepository(Company) private companiesRepository: Repository<Company>,
  ) {}

  async create(createJobDto: CreateJobDto) {
    try {
      const company = await this.companiesRepository.findOneBy({
        id: createJobDto.company,
      });
      delete createJobDto.company;
      const job = this.jobsRepository.create({
        name: createJobDto.name,
        description: createJobDto.description,
        skills: createJobDto.skills,
        salary: createJobDto.salary,
        quantity: createJobDto.quantity,
        location: createJobDto.location,
        level: createJobDto.level,
        startDate: createJobDto.startDate,
        endDate: createJobDto.endDate,
        isActive: createJobDto.isActive,
      });
      job.company = company;
      await this.jobsRepository.save(job);
      return 'OK';
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }

  async findAll(currentPage: number, limit: number, queryString: string) {
    const { filter } = aqp(queryString);
    delete filter.current;
    delete filter.pageSize;

    filter.skill = filter.skill ? filter.skill : '';

    filter.location = filter.location ? filter.location : '';

    filter.companyId = filter.companyId ? filter.companyId : false;

    filter.createdAt = filter.createdAt ? filter.createdAt : 'DESC';

    const defaultLimit = +limit ? +limit : 10;
    const offset = ((+currentPage || 1) - 1) * +defaultLimit;
    const totalItems = (
      filter.companyId
        ? await this.jobsRepository.find({
            where: {
              skills: Like(`%${filter.skill.trim()}%`),
              location: Like(`%${filter.location.trim()}%`),
              isActive: true,
              company: { id: filter.companyId },
            },
          })
        : await this.jobsRepository.find({
            where: {
              skills: Like(`%${filter.skill}%`),
              location: Like(`%${filter.location}%`),
              isActive: true,
            },
          })
    ).length;
    const totalPages = Math.ceil(totalItems / defaultLimit);

    const jobs = filter.companyId
      ? await this.jobsRepository.find({
          where: {
            skills: Like(`%${filter.skill}%`),
            location: Like(`%${filter.location}%`),
            isActive: true,
            company: { id: filter.companyId },
          },
          order: {
            createdAt: filter.createdAt,
          },
          skip: offset,
          take: defaultLimit,
          relations: ['company'],
        })
      : await this.jobsRepository.find({
          where: {
            skills: Like(`%${filter.skill}%`),
            location: Like(`%${filter.location}%`),
            isActive: true,
          },
          order: {
            createdAt: filter.createdAt,
          },
          skip: offset,
          take: defaultLimit,
          relations: ['company'],
        });
    return {
      data: jobs,
      meta: {
        current: currentPage || 1,
        pageSize: defaultLimit,
        pages: totalPages,
        total: totalItems,
      },
    };
  }

  async findAllJobs(currentPage: number, limit: number, queryString: string) {
    const { filter } = aqp(queryString);
    delete filter.current;
    delete filter.pageSize;

    const defaultLimit = +limit ? +limit : 10;
    const offset = ((+currentPage || 1) - 1) * +defaultLimit;
    const totalItems = (
      filter.companyId
        ? await this.jobsRepository.find({
            where: {
              company: { id: filter.companyId },
            },
          })
        : await this.jobsRepository.find()
    ).length;
    const totalPages = Math.ceil(totalItems / defaultLimit);

    const jobs = filter.companyId
      ? await this.jobsRepository.find({
          where: {
            company: { id: filter.companyId },
          },
          order: {
            createdAt: 'DESC',
          },
          skip: offset,
          take: defaultLimit,
          relations: ['company'],
        })
      : await this.jobsRepository.find({
          order: {
            createdAt: 'DESC',
          },
          skip: offset,
          take: defaultLimit,
          relations: ['company'],
        });
    return {
      data: jobs,
      meta: {
        current: currentPage || 1,
        pageSize: defaultLimit,
        pages: totalPages,
        total: totalItems,
      },
    };
  }

  async findOne(id: number) {
    try {
      const job = await this.jobsRepository.findOne({
        where: { id },
        relations: ['company', 'liked'],
      });
      return job;
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }

  async update(id: number, updateJobDto: UpdateJobDto) {
    try {
      const {
        name,
        description,
        salary,
        skills,
        quantity,
        level,
        startDate,
        endDate,
        location,
        isActive,
      } = updateJobDto;
      const job = await this.jobsRepository.findOneBy({ id });
      return await this.jobsRepository.save({
        ...job,
        name,
        description,
        salary,
        skills,
        quantity,
        level,
        startDate,
        endDate,
        location,
        isActive,
      });
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }

  remove(id: number) {
    try {
      this.jobsRepository.softDelete(id);
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }

  async like(id: number, user: IUser) {
    try {
      const job = await this.jobsRepository.findOne({
        where: { id },
        relations: ['liked'],
      });
      job.addUserLiked(user);
      return await this.jobsRepository.save(job);
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }

  async unlike(id: number, user: IUser) {
    try {
      const job = await this.jobsRepository.findOne({
        where: { id },
        relations: ['liked'],
      });
      job.liked = job.liked.filter((item) => item.id !== user.id);
      return await this.jobsRepository.save(job);
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }
}
