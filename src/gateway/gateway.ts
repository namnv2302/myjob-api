import { Company } from '@companies/entities/company.entity';
import { OnModuleInit } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import {
  WebSocketGateway,
  WebSocketServer,
  OnGatewayDisconnect,
  SubscribeMessage,
  MessageBody,
  ConnectedSocket,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { Repository } from 'typeorm';

@WebSocketGateway({ cors: '*' })
export class Gateway implements OnModuleInit, OnGatewayDisconnect {
  constructor(
    @InjectRepository(Company)
    private companiesRepository: Repository<Company>,
  ) {}

  private userList = [];

  @WebSocketServer()
  server: Server;

  onModuleInit() {
    this.server.on('connection', (socket) => {
      console.log('New connect: ', socket.id);
    });
  }

  handleDisconnect(client: Socket) {
    this.userList = this.userList.filter((user) => user.socketId !== client.id);
  }

  @SubscribeMessage('addNewUser')
  addNewUser(
    @MessageBody() data: { userId: string; fullname: string },
    @ConnectedSocket() client: Socket,
  ) {
    if (!this.userList.some((user) => `${user.userId}` === `${data.userId}`)) {
      this.userList.push({
        userId: data.userId,
        fullname: data.fullname,
        socketId: client.id,
      });
    }
  }

  async sendNotification(
    id: number,
    recipientId: number,
    senderId: number,
    status: number,
  ) {
    const recipientSocket = this.userList.find(
      (user) => `${user.userId}` === `${recipientId}`,
    );
    const sender = await this.companiesRepository.findOneBy({ id: senderId });
    if (recipientSocket) {
      this.server
        .to(recipientSocket.socketId)
        .emit('getNoti', { id, status, createdAt: new Date(), sender });
    }
  }
}
