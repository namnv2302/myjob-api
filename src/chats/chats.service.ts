import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateChatDto } from '@chats/dto/create-chat.dto';
import { UpdateChatDto } from '@chats/dto/update-chat.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Chat } from '@chats/entities/chat.entity';
import { In, Repository } from 'typeorm';

@Injectable()
export class ChatsService {
  constructor(
    @InjectRepository(Chat)
    private chatsRepository: Repository<Chat>,
  ) {}

  async create(createChatDto: CreateChatDto) {
    try {
      const { firstId, secondId } = createChatDto;
      const chatExisted = await this.chatsRepository.findOne({
        where: {
          firstId: In([firstId, secondId]),
          secondId: In([firstId, secondId]),
        },
      });
      if (chatExisted) {
        return chatExisted;
      }
      return await this.chatsRepository.save({
        firstId,
        secondId,
      });
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }

  findAll() {
    return `This action returns all chats`;
  }

  findOne(id: number) {
    return `This action returns a #${id} chat`;
  }

  async findUserChats(userId: string, currentPage: number, limit: number) {
    const defaultLimit = +limit ? +limit : 10;
    const offset = ((+currentPage || 1) - 1) * +defaultLimit;
    const totalItems = (
      await this.chatsRepository.find({
        where: [
          {
            firstId: userId,
          },
          {
            secondId: userId,
          },
        ],
        order: { sendLastAt: 'DESC' },
      })
    ).length;
    const totalPages = Math.ceil(totalItems / defaultLimit);

    try {
      const chats = await this.chatsRepository.find({
        where: [
          {
            firstId: userId,
          },
          {
            secondId: userId,
          },
        ],
        order: { sendLastAt: 'DESC' },
        skip: offset,
        take: defaultLimit,
      });
      return {
        data: chats,
        meta: {
          current: currentPage || 1,
          pageSize: defaultLimit,
          pages: totalPages,
          total: totalItems,
        },
      };
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }

  async findChat(firstId: string, secondId: string) {
    try {
      const chat = await this.chatsRepository.findOne({
        where: {
          firstId: In([firstId, secondId]),
          secondId: In([firstId, secondId]),
        },
      });
      return chat;
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }

  async update(id: number, updateChatDto: UpdateChatDto) {
    try {
      const chat = await this.chatsRepository.findOneBy({ id: id });
      if (chat) {
        return await this.chatsRepository.save({ ...chat, ...updateChatDto });
      }
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }

  remove(id: number) {
    return `This action removes a #${id} chat`;
  }
}
