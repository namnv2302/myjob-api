import { Module } from '@nestjs/common';
import { ChatsService } from '@chats/chats.service';
import { ChatsController } from '@chats/chats.controller';
import { Chat } from '@chats/entities/chat.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Chat])],
  controllers: [ChatsController],
  providers: [ChatsService],
  exports: [ChatsService],
})
export class ChatsModule {}
