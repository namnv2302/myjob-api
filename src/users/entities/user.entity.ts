import { Company } from '@/companies/entities/company.entity';
import { Notification } from '@/notifications/entities/notification.entity';
import { Post } from '@/posts/entities/post.entity';
import { Job } from '@jobs/entities/job.entity';
import { Resume } from '@resumes/entities/resume.entity';
import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  DeleteDateColumn,
  ManyToMany,
  OneToMany,
} from 'typeorm';

export enum GenderOption {
  MALE = 'male',
  FEMALE = 'female',
}

export enum Role {
  User = 'user',
  Employer = 'employer',
  Admin = 'admin',
}

@Entity('users')
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 100, nullable: true })
  fullname: string;

  @Column({ unique: true })
  email: string;

  @Column({ type: 'text', nullable: true, select: false })
  password: string;

  @Column({ type: 'enum', enum: GenderOption, default: GenderOption.FEMALE })
  gender: GenderOption;

  @Column({ nullable: true })
  phoneNumber: string;

  @Column({ type: 'text', nullable: true })
  avatar: string;

  @Column({ type: 'enum', enum: Role, default: Role.User })
  role: Role;

  @ManyToOne(() => Company, (company) => company.users)
  company: Company;

  @Column({ type: 'text', nullable: true, select: false })
  refreshToken: string;

  @Column({ type: 'boolean', nullable: true, default: false })
  isVerify: boolean;

  @ManyToMany(() => Job, (job) => job.liked)
  jobLiked: Job[];

  @ManyToMany(() => Company, (comp) => comp.followers)
  companiesFollowed: Company[];

  @OneToMany(() => Post, (post) => post.owner)
  posts: Post[];

  @ManyToMany(() => Post, (post) => post.likedList)
  likedPostList: Post[];

  @ManyToMany(() => Post, (post) => post.bookmarkList)
  bookmarkPostList: Post[];

  @OneToMany(() => Resume, (cv) => cv.sendBy)
  resumes: Resume;

  @OneToMany(() => Notification, (noti) => noti.recipient)
  notis: Notification;

  @CreateDateColumn({ nullable: true })
  createdAt: Date;

  @UpdateDateColumn({ nullable: true })
  updatedAt: Date;

  @DeleteDateColumn({ nullable: true })
  deletedAt: Date;
}
