import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import {
  CreateEmployerDto,
  CreateUserDto,
  SignUpEmployerDto,
} from '@users/dto/create-user.dto';
import { UpdateEmployerDto, UpdateUserDto } from '@users/dto/update-user.dto';
import { Role, User } from '@users/entities/user.entity';
import { Like, Repository } from 'typeorm';
import { compareSync, genSaltSync, hashSync } from 'bcryptjs';
import { Company } from '@companies/entities/company.entity';
import { MailService } from '@/mail/mail.service';
import aqp from 'api-query-params';
import { IUser } from '@users/users.interface';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private usersRepository: Repository<User>,
    @InjectRepository(Company) private companiesRepository: Repository<Company>,
    private mailService: MailService,
  ) {}

  getHashPassword(password: string) {
    const salt = genSaltSync(10);
    const hash = hashSync(password, salt);
    return hash;
  }

  isValidPassword(password: string, hash: string) {
    return compareSync(password, hash);
  }

  async updateRefreshToken(id: number, refreshToken: string) {
    return await this.usersRepository.update(id, {
      refreshToken,
    });
  }

  async create(createUserDto: CreateUserDto) {
    const userExist = await this.usersRepository.findOne({
      where: {
        email: createUserDto.email,
      },
      withDeleted: true,
    });
    if (userExist) {
      throw new BadRequestException('User already exist!');
    }
    try {
      await this.usersRepository.save({
        password: this.getHashPassword(createUserDto.password),
        fullname: createUserDto.fullname,
        email: createUserDto.email,
        gender: createUserDto.gender,
        phoneNumber: createUserDto.phoneNumber,
        avatar: createUserDto.avatar,
      });
      return 'Ok';
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }

  async createEmployer(createEmployerDto: CreateEmployerDto) {
    const userExist = await this.usersRepository.findOne({
      where: {
        email: createEmployerDto.email,
      },
      withDeleted: true,
    });
    if (userExist) {
      throw new BadRequestException('Employer already exist!');
    }
    try {
      const company = await this.companiesRepository.findOneBy({
        id: createEmployerDto.company,
      });

      const newEmployer = this.usersRepository.create({
        fullname: createEmployerDto.fullname,
        email: createEmployerDto.email,
        password: this.getHashPassword(createEmployerDto.password),
        phoneNumber: createEmployerDto.phoneNumber,
        gender: createEmployerDto.gender,
        avatar: createEmployerDto.avatar,
        role: createEmployerDto.role,
      });
      newEmployer.company = company;
      await this.usersRepository.save(newEmployer);
      return 'OK';
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }

  async signUpEmployer(createEmployerDto: SignUpEmployerDto) {
    const {
      email,
      fullname,
      password,
      phoneNumber,
      companyName,
      provinceName,
      districtName,
    } = createEmployerDto;
    const employerExist = await this.usersRepository.findOne({
      where: {
        email: email,
      },
      withDeleted: true,
    });
    if (employerExist) {
      throw new BadRequestException('Employer already exist!');
    }
    const companyExist = await this.companiesRepository.findOne({
      where: {
        companyName: companyName.toUpperCase(),
      },
      withDeleted: true,
    });
    if (companyExist) {
      throw new BadRequestException('Company already exist!');
    }
    try {
      const newCompany = await this.companiesRepository.save({
        companyName: companyName.toUpperCase(),
        provinceName,
        districtName,
      });
      const newEmployer = this.usersRepository.create({
        email,
        fullname,
        password: this.getHashPassword(password),
        phoneNumber,
        role: Role.Employer,
      });
      newEmployer.company = newCompany;
      await this.usersRepository.save(newEmployer);
      return 'OK';
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }

  async getMe(id: number) {
    try {
      const user = await this.usersRepository.findOne({
        where: { id },
        relations: ['company'],
      });
      return user;
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }

  async findAllUsersNormal(
    currentPage: number,
    limit: number,
    queryString: string,
  ) {
    const { filter } = aqp(queryString);
    delete filter.current;
    delete filter.pageSize;

    filter.fullname =
      filter.fullname && filter.fullname.trim() ? filter.fullname : '';

    const defaultLimit = +limit ? +limit : 10;
    const offset = ((+currentPage || 1) - 1) * +defaultLimit;
    const totalItems = (
      await this.usersRepository.find({
        where: {
          fullname: Like(`%${filter.fullname}%`),
          role: Role.User,
        },
      })
    ).length;
    const totalPages = Math.ceil(totalItems / defaultLimit);

    const users = await this.usersRepository.find({
      where: {
        fullname: Like(`%${filter.fullname}%`),
        role: Role.User,
      },
      order: {
        createdAt: 'DESC',
      },
      skip: offset,
      take: defaultLimit,
    });
    return {
      data: users,
      meta: {
        current: currentPage || 1,
        pageSize: defaultLimit,
        pages: totalPages,
        total: totalItems,
      },
    };
  }

  async findAllEmployer(
    currentPage: number,
    limit: number,
    queryString: string,
  ) {
    const { filter } = aqp(queryString);
    delete filter.current;
    delete filter.pageSize;

    filter.fullname =
      filter.fullname && filter.fullname.trim() ? filter.fullname : '';

    const defaultLimit = +limit ? +limit : 10;
    const offset = ((+currentPage || 1) - 1) * +defaultLimit;
    const totalItems = (
      await this.usersRepository.find({
        where: [
          {
            fullname: Like(`%${filter.fullname}%`),
            role: Role.Employer,
          },
          {
            fullname: Like(`%${filter.fullname}%`),
            role: Role.Admin,
          },
        ],
      })
    ).length;
    const totalPages = Math.ceil(totalItems / defaultLimit);

    const employers = await this.usersRepository.find({
      where: [
        {
          fullname: Like(`%${filter.fullname}%`),
          role: Role.Employer,
        },
        {
          fullname: Like(`%${filter.fullname}%`),
          role: Role.Admin,
        },
      ],
      order: {
        createdAt: 'DESC',
      },
      skip: offset,
      take: defaultLimit,
      relations: ['company'],
    });
    return {
      data: employers,
      meta: {
        current: currentPage || 1,
        pageSize: defaultLimit,
        pages: totalPages,
        total: totalItems,
      },
    };
  }

  async findOne(id: number) {
    try {
      const user = await this.usersRepository.findOne({
        where: { id },
        relations: ['company'],
      });
      return user;
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }

  async jobLiked(id: number) {
    try {
      const user = await this.usersRepository.findOne({
        where: { id },
        relations: ['jobLiked'],
      });
      return user.jobLiked;
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }

  async companiesFollowed(id: number) {
    try {
      const user = await this.usersRepository.findOne({
        where: { id },
        relations: ['companiesFollowed'],
      });
      return user.companiesFollowed;
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }

  findOneByUsername(username: string) {
    return this.usersRepository.findOne({
      where: { email: username },
      select: ['id', 'email', 'password', 'role'],
    });
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    try {
      const user = await this.usersRepository.findOneBy({ id });

      return await this.usersRepository.save({
        ...user,
        fullname: updateUserDto.fullname,
        gender: updateUserDto.gender,
        phoneNumber: updateUserDto.phoneNumber,
        avatar: updateUserDto.avatar,
        isVerify: !!updateUserDto.isVerify,
      });
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }

  async passwordChange(email: string, currentPw: string, newPw: string) {
    const user = await this.findOneByUsername(email);
    const isCorrect = this.isValidPassword(currentPw, user.password);
    if (isCorrect) {
      return await this.usersRepository.save({
        ...user,
        password: this.getHashPassword(newPw),
      });
    } else {
      throw new BadRequestException('Current password not correct');
    }
  }

  async updateByAdmin(id: number, updateUserDto: UpdateUserDto) {
    try {
      const user = await this.usersRepository.findOneBy({ id });
      return await this.usersRepository.save({
        ...user,
        ...updateUserDto,
      });
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }

  async updateEmployer(id: number, updateEmployerDto: UpdateEmployerDto) {
    try {
      const company = await this.companiesRepository.findOneBy({
        id: updateEmployerDto.company,
      });
      const user = await this.usersRepository.findOneBy({ id });
      user.company = company;
      return await this.usersRepository.save({
        ...user,
        fullname: updateEmployerDto.fullname,
        gender: updateEmployerDto.gender,
        role: updateEmployerDto.role,
        phoneNumber: updateEmployerDto.phoneNumber,
        avatar: updateEmployerDto.avatar,
      });
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }

  remove(id: number) {
    try {
      this.usersRepository.softDelete(id);
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }

  async bookmark(user: IUser) {
    try {
      const users = await this.usersRepository.findOne({
        where: { id: user.id },
        relations: ['bookmarkPostList', 'bookmarkPostList.owner'],
      });
      return { data: users.bookmarkPostList };
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }
}
