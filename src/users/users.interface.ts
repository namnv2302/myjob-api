export interface IUser {
  id: number;
  name: string;
  email: string;
  password: string;
  avatar: string;
  role: string;
  refreshToken: string;
  createdAt: Date;
  updatedAt: Date;
}
