import { ApiProperty } from '@nestjs/swagger';
import { GenderOption, Role } from '@users/entities/user.entity';
import { IsNotEmpty, IsEmail } from 'class-validator';

export class CreateUserDto {
  @IsNotEmpty()
  fullname: string;

  @IsNotEmpty()
  @IsEmail()
  email: string;

  @IsNotEmpty()
  password: string;

  phoneNumber: string;

  avatar: string;

  gender: GenderOption;

  role: Role;

  isVerify: boolean;
}

export class UserLoginDto {
  @IsNotEmpty()
  @IsEmail()
  @ApiProperty({ example: 'user1@example.com', description: 'username' })
  username: string;

  @IsNotEmpty()
  @ApiProperty({ example: '123456', description: 'password' })
  password: string;
}

export class SignUpEmployerDto {
  @IsNotEmpty()
  fullname: string;

  @IsNotEmpty()
  @IsEmail()
  email: string;

  @IsNotEmpty()
  password: string;

  phoneNumber: string;

  avatar: string;

  gender: GenderOption;

  role: Role;

  @IsNotEmpty()
  companyName: string;

  @IsNotEmpty()
  provinceName: string;

  @IsNotEmpty()
  districtName: string;

  logo: string;

  scales: string;
}

export class CreateEmployerDto {
  @IsNotEmpty()
  fullname: string;

  @IsNotEmpty()
  @IsEmail()
  email: string;

  @IsNotEmpty()
  password: string;

  phoneNumber: string;

  avatar: string;

  gender: GenderOption;

  role: Role;

  company: number;
}
