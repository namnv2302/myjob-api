import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Query,
} from '@nestjs/common';
import { UsersService } from '@users/users.service';
import { CreateUserDto, CreateEmployerDto } from '@users/dto/create-user.dto';
import { UpdateEmployerDto, UpdateUserDto } from '@users/dto/update-user.dto';
import { Public } from '@/decorator/customize';
import { ApiTags } from '@nestjs/swagger';
import { RolesGuard } from '@/guards/roles.guard';
import { Roles } from '@/decorator/roles';
import { Role } from '@users/entities/user.entity';
import { User } from '@/decorator/user';
import { IUser } from '@users/users.interface';

@ApiTags('users')
@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Public()
  @Post()
  create(@Body() createUserDto: CreateUserDto) {
    return this.usersService.create(createUserDto);
  }

  @Post('employer')
  @UseGuards(RolesGuard)
  @Roles(Role.Admin)
  createEmployer(@Body() createEmployerDto: CreateEmployerDto) {
    return this.usersService.createEmployer(createEmployerDto);
  }

  @Get('normal')
  @UseGuards(RolesGuard)
  @Roles(Role.Admin)
  findAllUsersNormal(
    @Query('current') currentPage: string,
    @Query('limit') limit: string,
    @Query() queryString: string,
  ) {
    return this.usersService.findAllUsersNormal(
      +currentPage,
      +limit,
      queryString,
    );
  }

  @Get('employer')
  @UseGuards(RolesGuard)
  @Roles(Role.Admin)
  findAllEmployer(
    @Query('current') currentPage: string,
    @Query('limit') limit: string,
    @Query() queryString: string,
  ) {
    return this.usersService.findAllEmployer(+currentPage, +limit, queryString);
  }

  @Public()
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.usersService.findOne(+id);
  }

  @Get(':id/jobLiked')
  jobLiked(@Param('id') id: string) {
    return this.usersService.jobLiked(+id);
  }

  @Get(':id/companiesFollowed')
  companiesFollowed(@Param('id') id: string) {
    return this.usersService.companiesFollowed(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
    return this.usersService.update(+id, updateUserDto);
  }

  @Patch(':email/pw-change')
  passwordChange(
    @Param('email') email: string,
    @Body('currentPw') currentPw: string,
    @Body('newPw') newPw: string,
  ) {
    return this.usersService.passwordChange(email, currentPw, newPw);
  }

  @Patch(':id/admin')
  @UseGuards(RolesGuard)
  @Roles(Role.Admin)
  updateByAdmin(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
    return this.usersService.updateByAdmin(+id, updateUserDto);
  }

  @Patch(':id/employer')
  @UseGuards(RolesGuard)
  @Roles(Role.Admin)
  updateEmployer(
    @Param('id') id: string,
    @Body() updateEmployerDto: UpdateEmployerDto,
  ) {
    return this.usersService.updateEmployer(+id, updateEmployerDto);
  }

  @Delete(':id')
  @UseGuards(RolesGuard)
  @Roles(Role.Admin)
  remove(@Param('id') id: string) {
    return this.usersService.remove(+id);
  }

  @Get('bookmark/posts')
  bookmark(@User() user: IUser) {
    return this.usersService.bookmark(user);
  }
}
