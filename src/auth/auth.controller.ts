import {
  Body,
  Controller,
  Get,
  Post,
  Req,
  Res,
  UseGuards,
} from '@nestjs/common';
import { AuthService } from '@auth/auth.service';
import { LocalAuthGuard } from '@/guards/local-auth.guard';
import { Public } from '@/decorator/customize';
import { User } from '@/decorator/user';
import { IUser } from '@users/users.interface';
import {
  CreateUserDto,
  SignUpEmployerDto,
  UserLoginDto,
} from '@users/dto/create-user.dto';
import { ThrottlerGuard } from '@nestjs/throttler';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { GoogleAuthGuard } from '@/guards/google-auth.guard';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Public()
  @UseGuards(LocalAuthGuard)
  @UseGuards(ThrottlerGuard)
  @ApiBody({ type: UserLoginDto })
  @Post('login')
  login(@Req() req) {
    return this.authService.login(req.user);
  }

  @Public()
  @Get('refresh-token')
  refreshToken(@Req() req) {
    const refreshToken = req.headers.authorization.split(' ')[1];
    return this.authService.refreshToken(refreshToken);
  }

  @Get('me')
  whoAmI(@User() user: IUser) {
    return this.authService.getMe(user);
  }

  @Public()
  @Post('sign-up')
  signUp(@Body() createUserDto: CreateUserDto) {
    return this.authService.signUp(createUserDto);
  }

  @Public()
  @Post('sign-up/employer')
  signUpEmployer(@Body() createEmployerDto: SignUpEmployerDto) {
    return this.authService.signUpEmployer(createEmployerDto);
  }

  @Post('verify-email')
  verifyEmail(@Body('postcode') postcode: string, @User() user: IUser) {
    return this.authService.verifyEmail(+postcode, user);
  }

  @Get('logout')
  logout(@User() user: IUser) {
    return this.authService.logout(user);
  }

  @Public()
  @UseGuards(GoogleAuthGuard)
  @Get('google')
  googleLogin() {
    return 'google login';
  }

  @Public()
  @UseGuards(GoogleAuthGuard)
  @Get('google/callback')
  googleCallback(@Req() req: any, @Res({ passthrough: true }) res: any) {
    return this.authService.googleCallback(req.user, res);
  }
}
