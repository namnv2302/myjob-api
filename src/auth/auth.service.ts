import {
  BadRequestException,
  HttpException,
  HttpStatus,
  Injectable,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { IUser } from '@users/users.interface';
import { UsersService } from '@users/users.service';
import ms from 'ms';
import { CreateUserDto, SignUpEmployerDto } from '@users/dto/create-user.dto';
import { MailService } from '@/mail/mail.service';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
    private configService: ConfigService,
    private mailService: MailService,
  ) {}

  createRefreshToken(payload) {
    const refreshToken = this.jwtService.sign(payload, {
      secret: this.configService.get<string>('REFRESH_TOKEN_SECRET'),
      expiresIn:
        ms(this.configService.get<string>('REFRESH_TOKEN_EXPIRE')) / 1000,
    });
    return refreshToken;
  }

  async validateUser(username: string, password: string): Promise<any> {
    const user = await this.usersService.findOneByUsername(username);
    if (user) {
      const isMatchPassword = this.usersService.isValidPassword(
        password,
        user.password,
      );
      if (isMatchPassword) {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const { password, ...result } = user;
        return result;
      }
    }
    return null;
  }

  async login(user: IUser) {
    const payload = {
      sub: 'token login',
      iss: 'from server',
      id: user.id,
      email: user.email,
      role: user.role,
    };

    const refreshToken = this.createRefreshToken(payload);
    await this.usersService.updateRefreshToken(user.id, refreshToken);

    return {
      accessToken: this.jwtService.sign(payload),
      refreshToken,
    };
  }

  async refreshToken(refreshToken: string) {
    try {
      const decode = this.jwtService.verify(refreshToken, {
        secret: this.configService.get<string>('REFRESH_TOKEN_SECRET'),
      });
      const user = await this.usersService.findOneByUsername(decode.email);
      if (user) {
        const payload = {
          sub: 'token refresh',
          iss: 'from server',
          id: user.id,
          email: user.email,
          role: user.role,
        };
        const refreshToken = this.createRefreshToken(payload);
        await this.usersService.updateRefreshToken(user.id, refreshToken);

        return {
          accessToken: this.jwtService.sign(payload),
          refreshToken,
        };
      } else {
        throw new BadRequestException('Refresh token invalid. Login again!');
      }
    } catch (error) {
      throw new BadRequestException('Refresh token invalid. Login again!');
    }
  }

  getMe(user: IUser) {
    return this.usersService.getMe(user.id);
  }

  signUp(createUserDto: CreateUserDto) {
    return this.usersService.create(createUserDto);
  }

  signUpEmployer(createEmployerDto: SignUpEmployerDto) {
    return this.usersService.signUpEmployer(createEmployerDto);
  }

  async verifyEmail(postcode: number, user: IUser) {
    const result = this.mailService.verifyPostcode(postcode);
    if (result) {
      const findUser = await this.usersService.findOne(user.id);
      if (findUser.isVerify) {
        throw new HttpException('Email verified!!', HttpStatus.BAD_REQUEST);
      }
      return await this.usersService.update(findUser.id, { isVerify: true });
    } else {
      throw new BadRequestException('Postcode not correct!!');
    }
  }

  async logout(user: IUser) {
    await this.usersService.updateRefreshToken(user.id, null);
    return 'OK';
  }

  googleCallback(user: IUser, res: any) {
    res.redirect(
      `${this.configService.get<string>('CLIENT_URL')}/google-success?userId=${
        user.id
      }`,
    );
  }
}
