import { Strategy } from 'passport-google-oauth20';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '@users/entities/user.entity';
import { Repository } from 'typeorm';
import { UsersService } from '@users/users.service';

@Injectable()
export class GoogleStrategy extends PassportStrategy(Strategy) {
  constructor(
    private configService: ConfigService,
    @InjectRepository(User) private usersRepository: Repository<User>,
    private usersService: UsersService,
  ) {
    super({
      clientID: configService.get<string>('GOOGLE_CLIENT_ID'),
      clientSecret: configService.get<string>('GOOGLE_CLIENT_SECRET'),
      callbackURL: `${configService.get<string>(
        'BACKEND_URL',
      )}/api/auth/google/callback`,
      scope: ['email', 'profile'],
    });
  }

  async validate(
    accessToken: string,
    refreshToken: string,
    profile: any,
    done: any,
  ) {
    const user = await this.usersRepository.findOne({
      where: { email: profile.emails[0].value },
    });
    if (user) {
      done(null, user);
    } else {
      const newUser = await this.usersRepository.save({
        email: profile.emails[0].value,
        fullname: profile.displayName,
        avatar: profile.photos[0].value,
        password: this.usersService.getHashPassword(
          `${this.configService.get<string>('DEFAULT_PW')}`,
        ),
        isVerify: true,
      });
      done(null, newUser);
    }
  }
}
