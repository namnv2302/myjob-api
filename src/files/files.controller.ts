import {
  Body,
  Controller,
  HttpStatus,
  ParseFilePipeBuilder,
  Post,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { FilesService } from '@/files/files.service';
import { CloudinaryService } from '@/cloudinary/cloudinary.service';
import { Public } from '@/decorator/customize';
import { FileInterceptor } from '@nestjs/platform-express';

@Controller('files')
export class FilesController {
  constructor(
    private readonly filesService: FilesService,
    private readonly cloudinaryService: CloudinaryService,
  ) {}

  @Public()
  @Post('upload')
  @UseInterceptors(FileInterceptor('file'))
  uploadFile(
    @UploadedFile(
      new ParseFilePipeBuilder()
        .addFileTypeValidator({
          fileType:
            /^(application\/msword|image\/jpeg|image\/png|application\/pdf|text\/plain|image\/webp|application\/vnd.openxmlformats-officedocument.wordprocessingml.document)$/i,
        })
        .addMaxSizeValidator({ maxSize: 1024 * 1024 * 10 }) // 10mb
        .build({ errorHttpStatusCode: HttpStatus.UNPROCESSABLE_ENTITY }),
    )
    file: Express.Multer.File,
    @Body('folderName') folderName: string,
  ) {
    if (!folderName) folderName = '';
    return this.cloudinaryService.uploadFile(file, folderName);
  }
}
