import { Body, Controller, Post } from '@nestjs/common';
import { MailService } from '@/mail/mail.service';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('mail')
@Controller('mail')
export class MailController {
  constructor(private readonly mailService: MailService) {}

  @Post('verify-email')
  sendVerifyEmail(
    @Body('email') email: string,
    @Body('fullname') fullname: string,
  ) {
    return this.mailService.sendVerifyEmail(email, fullname);
  }
}
