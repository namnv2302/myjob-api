FROM node:14-alpine

WORKDIR /usr/myjob-backend

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run build

EXPOSE 8000
CMD ["node", "dist/main.js"]